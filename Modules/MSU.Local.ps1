Param(
	[Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [PSObject]$Session,
	[Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [PSObject]$Facts,
	[Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
	[string]$Type,
	[Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
	[string[]]$Values,
	[Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
	[switch]$CheckOnly = $False
)

Function Search-MSBulletin {
  Param(
    [Parameter(Mandatory=$True,
               ValueFromPipeline=$True,
               ValueFromPipelineByPropertyName=$True)]
    [string]$WindowsVersion,
    [Parameter(Mandatory=$True,
               ValueFromPipeline=$True,
               ValueFromPipelineByPropertyName=$True)]
    [string]$Bit,
    [ValidateSet('ID','MSB','MonthlySummary','ConvenienceRollup','Cumulative','File')]
    [Parameter(Mandatory=$True,
               ValueFromPipeline=$True,
               ValueFromPipelineByPropertyName=$True)]
    [string]$Type,
    [Parameter(ValueFromPipeline=$True,
               ValueFromPipelineByPropertyName=$True)]
    [string]$Value,
    [Parameter(ValueFromPipeline=$True,
               ValueFromPipelineByPropertyName=$True)]
    [string]$Repo = ""
  )
  Begin {
    Try {
      $TestRepo = Test-Path $Repo -ErrorAction Stop
    } Catch {
      Write-Error "Failed to locate patch repo: $Repo"
    }
  }
  Process {
    $Properties = @{
      Name = $Null
      Path = $Null
      ID = $Null
      Errors = $Null
    }
    Try {
      If ($Type) {
        Switch ($Type) {
          'KB' {
            $KB = $Value
          }
          'MSB' {
            $MSB = $Value.ToUpper()
            $MSYear = "20$($MSB.Replace('MS','').Split('-')[0])"
            $Repo = "$Repo\$MSYear\$MSB"
          }
          'MonthlySummary' {
            $Repo = "$Repo\Monthly Summary\$Value"
          }
          'ConvenienceRollup' {
            $Repo = "$Repo\Convenience Rollup"
          }
          'Cumulative' {
            $Repo = "$Repo\Cumulative"
          }
          'File' {$Repo = $Value}
        }
      }

      # Search the SearchLocation for matches
      $Patches = Get-ChildItem -Path $Repo -Force -Recurse -File "*$WindowsVersion*$KB*$Bit*" -ErrorAction SilentlyContinue

    } Catch {
        $Properties.Errors = "Failed to select a matching patch"
    } Finally {
      ForEach ($Patch in $Patches) {
        $Properties.Path = $Patch.FullName
        $Properties.Name = $Patch.Name
        $Properties.ID = ($Patch.Name.Split('-') | Select-String KB).ToString().ToUpper()
        $Obj = New-Object -TypeName PSObject -Property $Properties
        Write-Output $Obj | Select Name,Path,ID,Errors
      }
    }
  }
  End {}
}


ForEach ($Value in $Values) {
	$ErrorsCount = $Global:Error.Count
	Try {
		$Properties = @{}

		$SearchMSBulletin = Search-MSBulletin -WindowsVersion $Facts.WindowsVersion -Bit $Facts.Bit -Type $Type -Value $Value
  	$Properties.Patch = $SearchMSBulletin.Name
  	$Properties.Path = $SearchMSBulletin.Path
  	$Properties.ID = $SearchMSBulletin.ID

		If ($Properties.ID) {
			If ($Facts.HotFix -Contains $Properties.ID) {
				$Properties.Installed = $True
				$Output = @{
					Changed = $False
					Success = $True
					Errors = @()
					Output = "$($Properties.ID): Already Installed"
				}
			} Else {
				$Properties.Installed = $False
				If (!($CheckOnly)) {
					Copy-Item	-ToSession $Session -Force -Path $Properties.Path -Destination 'C:\Temp\' -ErrorAction Stop | Out-Null
					$Arguments = @{FileName = "C:\Temp\$($Properties.Patch)"}
					$Arguments = New-Object -TypeName PSObject -Property $Arguments
					$Results = Invoke-RemoteScript -Session $Session -Module MSU -Arguments $Arguments
					$Output = @{
						Changed = $Results.Changed
						Success = $Results.Success
						Errors = $Results.Errors
						Output = "$($Properties.ID): $($Results.Output)"
					}
				} Else {
					$Output = @{
						Changed = $False
						Success = $True
						Errors = @()
						Output = "$($Properties.ID): Not Installed"
					}
				}
			}
		} Else {
			$Output = @{
				Changed = $False
				Success = $True
				Errors = @()
				Output = "$Value : No System Match"
			}
		}
	} Catch {
		$Output = @{
    	Changed = $False
    	Success = $False
    	Errors = $Global:Error[0..$($ErrorsCount - $Global:Error.Count)]
    	Output = "$Value : Error: $($Global:Error[0].FullyQualifiedErrorID)"
    }
	} Finally {
		$Obj = New-Object -TypeName PSObject -Property $Output
  	$Obj
	}
}
