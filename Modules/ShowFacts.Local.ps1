Param(
	[Parameter(
		Mandatory=$True,
		ValueFromPipeline=$True,
		ValueFromPipelineByPropertyName=$True)]
	[PSObject]$Computer,
	[Parameter(
		Mandatory=$True,
		ValueFromPipeline=$True,
		ValueFromPipelineByPropertyName=$True)]
	[string[]]$Property
)

$Props = $Property
ForEach ($Prop in $Props) {
	$Changed = $False
	$Success = $False
	$Errors = @()
	$ErrorsCount = $Global:Error.Count
	$Output = @()

	Try {
		$Output += "$($Prop): $($Computer.Facts.$Prop)"
		$Success = $True
	}
	Catch {
		$Errors = $Global:Error[0..$($ErrorsCount - $Global:Error.Count)]
	}
	Finally {
		$Properties = @{
			Changed = $Changed
			Success = $Success
			Errors = $Errors
			Output = $Output
		}
		$Obj = New-Object -TypeName PSObject -Property $Properties
		$Obj
	}
}
