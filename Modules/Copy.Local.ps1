Param(
	[Parameter(
		Mandatory=$True,
		ValueFromPipeline=$True,
		ValueFromPipelineByPropertyName=$True)]
  [PSObject]$Computer,
	[Parameter(
		Mandatory=$True,
		ValueFromPipeline=$True,
		ValueFromPipelineByPropertyName=$True)]
	[string[]]$FileName,
	[Parameter(
		Mandatory=$True,
		ValueFromPipeline=$True,
		ValueFromPipelineByPropertyName=$True)]
	[string]$Destination,
	[Parameter(
		ValueFromPipeline=$True,
		ValueFromPipelineByPropertyName=$True)]
	[bool]$Force = $False
)

$FileNames = $FileName
ForEach ($FileName in $FileNames) {
	$Changed = $False
	$Success = $False
	$Errors = @()
	$ErrorsCount = $Global:Error.Count
	$Output = @()

	Try {
		Try {
			$FileProp = Get-ItemProperty "$FileName" -ErrorAction Stop
		}
		Catch {
			$Errors += "Failed to find: $FileName"
			Return
		}
		Try {
			$TestDestination = Invoke-Command -Session $Computer.Session -ArgumentList $Destination -ScriptBlock {
				Param($Destination)
				If (!(Test-Path $Destination)) {
					New-Item -Path $Destination -Force -Type Directory
				}
			}
			If ($Force) {
				Copy-Item	-ToSession $Computer.Session -Force	-Recurse -LiteralPath $FileProp.FullName	-Destination $Destination	-ErrorAction Stop | Out-Null
			} Else {
				Copy-Item	-ToSession $Computer.Session -Recurse	-LiteralPath $FileProp.FullName -Destination $Destination -ErrorAction Stop | Out-Null
			}
			$Output += "Copy Successful"
			$Success = $True
			$Changed = $True
		} Catch {
			Throw
		}
	}
	Catch {
		$Errors = $Global:Error[0..$($ErrorsCount - $Global:Error.Count)]
	}
	Finally {
		$Properties = @{
			Changed = $Changed
			Success = $Success
			Errors = $Errors
			Output = $Output
		}
		$Obj = New-Object -TypeName PSObject -Property $Properties
		$Obj
	}
}
