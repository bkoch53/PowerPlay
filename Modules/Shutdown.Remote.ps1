Param(
  [Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [ValidateSet('Shutdown','Restarted','Aborted')]
  [string]$State = "Restarted",
  [Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [string]$Comment = "The system is shutting down now",
  [Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [int]$Timeout = 600,
  [Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [bool]$Force = $False,
  [Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [bool]$Scheduled = $False,
  [Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [string]$StartTime = "00:00",
  [Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [bool]$UptimeCheck = $False,
  [Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [int]$Uptime = 1,
  [Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [bool]$DayOfWeekCheck = $False,
  [Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [ValidateSet('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday')]
  [string]$DayOfWeek = "Wednesday",
  [Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [bool]$OUCheck = $False,
  [Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [string]$OU
)

$ErrorsCount = $Global:Errors.Count

$Properties = @{
  Date = Get-Date -F "ddMMMyy HH:mm"
  ComputerName = hostname
  State = $State
  Comment = $Comment
  Timeout = $Timeout
  Force = $Force
  Scheduled = $Scheduled
  StartTime = $StartTime
  UptimeCheck = $UptimeCheck
  Uptime = $Uptime
  DayOfWeekCheck = $DayOfWeekCheck
  DayOfWeek = $DayOfWeek
  OUCheck = $OUCheck
  OU = $OU
  Changed = $False
	Success = $False
	Errors = @()
	Output = @()
}


Try {
	If ($OUCheck) {
		$objSearcher = New-Object System.DirectoryServices.DirectorySearcher
		$objSearcher.SearchRoot = New-Object System.DirectoryServices.DirectoryEntry
		$objSearcher.Filter = "(&(objectCategory=Computer)(SamAccountname=$($env:COMPUTERNAME)`$))"
		$objSearcher.SearchScope = "Subtree"
		$obj = $objSearcher.FindOne()
		$DN = $obj.Properties["distinguishedname"]

		If ($DN -notlike "*,$OU") {
			$Properties.Errors += "OUCheck Failed: $DN"
			$Properties.Output += "OUCheck Failed"
			return
		}
	}

	If ($DayOfWeekCheck) {
		If (!((Get-Date).DayOfWeek -Contains $DayOfWeek)) {
			$Properties.Errors += "DayOfWeekCheck Failed: $((Get-Date).DayOfWeek)"
			$Properties.Output += "DayOfWeekCheck Failed"
			return
		}
	}


	If ($Scheduled) {
		$StartTime = Get-Date $StartTime
		$EndTime = (Get-Date).AddHours(3)
		While (!($StartTime -le (Get-Date) -and $EndTime -ge (Get-Date))) {
			Start-Sleep 60
		}
	}

	If ($UptimeCheck) {
		$TimeDataObject=Get-WmiObject -Class Win32_OperatingSystem -Property LastBootUpTime, LocalDateTime -ErrorAction SilentlyContinue
		if ($TimeDataObject) {
				# Calculate system up time from WMI
				$LocalTime=$TimeDataObject.ConvertToDateTime($TimeDataObject.LocalDateTime)
				$BootUpTime=$TimeDataObject.ConvertToDateTime($TimeDataObject.LastBootUpTime)
				$SystemUpTime=($LocalTime - $BootUpTime)
		} else {
				# Unable to get WMI object; initialize variable to zero up time.
				# This prevents reboots unless the threshold is also zero.
				$SystemUpTime=(New-TimeSpan)
		}
		If (!(($SystemUpTime.Days) -ge $Uptime)) {
			$Properties.Errors += "UptimeCheck Failed: $($SystemUpTime.Days)"
      $Properties.Output += "UptimeCheck Failed"
      return
		}
	}

	$Comment = $Comment + " : Scheduled for $((Get-Date).AddSeconds($Timeout).ToShortTimeString())"

	Switch ($State) {
		'Shutdown' {
			Switch ($Force) {
				$True  {
					shutdown.exe /a 2>$Null
					shutdown.exe /c $Comment /t $Timeout /s /f 2>$Null
				}
				$False {
					shutdown.exe /c $Comment /t $Timeout /s 2>$Null
				}
			}
		}
		'Restarted' {
			Switch ($Force) {
				$True  {
					shutdown.exe /a 2>$Null
					shutdown.exe /c $Comment /t $Timeout /r /f 2>$Null
				}
				$False {
					shutdown.exe /c $Comment /t $Timeout /r 2>$Null
				}
			}
		}
		'Abort' {
			shutdown.exe /a 2>$Null
		}
	}
	Switch ($LASTEXITCODE) {
		0 {
			$Properties.Output += "Ok"
			$Properties.Success = $True
			$Properties.Changed = $True
		}
		Default {
			$Properties.Output += "Failed"
			$Properties.Errors = $Global:Error[0..$($ErrorsCount - $Global:Error.Count)]
		}
	}
}
Catch {
	$Properties.Output += "Failed"
	$Properties.Errors = $Global:Error[0..$($ErrorsCount - $Global:Error.Count)]
}

Finally {
	$Obj = New-Object -TypeName PSObject -Property $Properties
	$Obj
}
