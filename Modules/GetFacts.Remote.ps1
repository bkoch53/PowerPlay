Function Get-InstalledProgram {
    [CmdletBinding(SupportsShouldProcess=$true)]
    param(
        [Parameter(ValueFromPipeline              =$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0
        )]
        [string[]]
            $ComputerName = $env:COMPUTERNAME,
        [Parameter(Position=0)]
        [string[]]
            $Property,
        [switch]
            $ExcludeSimilar,
        [int]
            $SimilarWord
    )

    begin {
        $RegistryLocation = 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\',
                            'SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\'
        $HashProperty = @{}
        $SelectProperty = @('ProgramName','ComputerName')
        if ($Property) {
            $SelectProperty += $Property
        }
    }

    process {
        foreach ($Computer in $ComputerName) {
            $RegBase = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey([Microsoft.Win32.RegistryHive]::LocalMachine,$Computer)
            $RegistryLocation | ForEach-Object {
                $CurrentReg = $_
                if ($RegBase) {
                    $CurrentRegKey = $RegBase.OpenSubKey($CurrentReg)
                    if ($CurrentRegKey) {
                        $CurrentRegKey.GetSubKeyNames() | ForEach-Object {
                            if ($Property) {
                                foreach ($CurrentProperty in $Property) {
                                    $HashProperty.$CurrentProperty = ($RegBase.OpenSubKey("$CurrentReg$_")).GetValue($CurrentProperty)
                                }
                            }
                            $HashProperty.ComputerName = $Computer
                            $HashProperty.ProgramName = ($DisplayName = ($RegBase.OpenSubKey("$CurrentReg$_")).GetValue('DisplayName'))
                            if ($DisplayName) {
                                New-Object -TypeName PSCustomObject -Property $HashProperty |
                                Select-Object -Property $SelectProperty
                            }
                        }
                    }
                }
            } | ForEach-Object -Begin {
                if ($SimilarWord) {
                    $Regex = [regex]"(^(.+?\s){$SimilarWord}).*$|(.*)"
                } else {
                    $Regex = [regex]"(^(.+?\s){3}).*$|(.*)"
                }
                [System.Collections.ArrayList]$Array = @()
            } -Process {
                if ($ExcludeSimilar) {
                    $null = $Array.Add($_)
                } else {
                    $_
                }
            } -End {
                if ($ExcludeSimilar) {
                    $Array | Select-Object -Property *,@{
                        name       = 'GroupedName'
                        expression = {
                            ($_.ProgramName -split $Regex)[1]
                        }
                    } |
                    Group-Object -Property 'GroupedName' | ForEach-Object {
                        $_.Group[0] | Select-Object -Property * -ExcludeProperty GroupedName
                    }
                }
            }
        }
    }
}

$Properties = @{}
$WmiObj = Get-WmiObject -Class Win32_OperatingSystem -Property CSName,OSArchitecture,Version | Select CSName,OSArchitecture,Version
$Properties.OSArchitechture = $WmiObj.OSArchitecture
$Properties.Version = $WmiObj.Version
$Properties.Model = Get-WmiObject -Class Win32_ComputerSystem -Property Model | Select -Expand Model
$Properties.InstalledProgram = Get-InstalledProgram -Property DisplayVersion | Select @{Name="Name";Expression={$_."ProgramName"}}, @{Name="Version";Expression={$_."DisplayVersion"}}
$Properties.HotFix = Get-HotFix | Select -Expand HotFixID

If ($WMIObj.OSArchitecture -eq "64-bit") {
	$Properties.Bit = "x64"
} Else {
  $Properties.Bit = "x86"
}
Switch -Wildcard ($WMIObj.Version) {
	"5.2*" {$Properties.WindowsVersion = "WindowsServer2003"; Break}
  "6.0*" {$Properties.WindowsVersion = "Windows6.0"; Break}
  "6.1*" {$Properties.WindowsVersion = "Windows6.1"; Break}
  "6.2*" {$Properties.WindowsVersion = "Windows8-RT"; Break}
  "6.3*" {$Properties.WindowsVersion = "Windows8.1"; Break}
  "10*"  {$Properties.WindowsVersion = "Windows10"}
}

# Get Last Loggedon User
$WQLFilter = "NOT SID = 'S-1-5-18' AND NOT SID = 'S-1-5-19' AND NOT SID = 'S-1-5-20'"
$Win32User = Get-WmiObject -Class Win32_UserProfile -Filter $WQLFilter
$LastUser = $Win32User | Sort-Object -Property LastUseTime -Descending | Select-Object -First 1
$Loaded = $LastUser.Loaded
$UserSID = New-Object System.Security.Principal.SecurityIdentifier($LastUser.SID)
$Properties.LastLoggedonUser = $UserSID.Translate([System.Security.Principal.NTAccount]).Value.Split('\')[-1]

# Get SDC (OEM) Info
$Properties.OEMInfo = (Get-ItemProperty "Registry::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation\").Model

$Obj = New-Object -TypeName PSObject -Property $Properties
Write-Output $Obj
