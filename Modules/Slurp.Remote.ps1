Param(
	[Parameter(
	    ValueFromPipeline=$True,
		ValueFromPipelineByPropertyName=$True)]
	[string[]]$FileName,
	[Parameter(
	    ValueFromPipeline=$True,
		ValueFromPipelineByPropertyName=$True)]
	[bool]$Encoded = $False
)

ForEach ($File in $FileName) {
	$Changed = $False
	$Success = $False
	$Errors = @()
	$ErrorsCount = $Global:Error.Count
	$Output = @()
	Try {
		$File = Get-ItemProperty $File -ErrorAction Stop
		If ($Encoded) {
			$Bytes = [System.IO.File]::ReadAllBytes($File.FullName)
	    $Output += [System.Convert]::ToBase64String($Bytes)
		} Else {
			$Output += [System.IO.File]::ReadAllText($File.FullName)
		}
		$Success = $True
	} Catch {
		$Output = "Failed to slurp: $File"
  	$Errors = $Global:Error[0..$($ErrorsCount - $Global:Error.Count)]
	} Finally {
		$Properties = @{
			Changed = $Changed
			Success = $Success
			Errors = $Errors
			Output = $Output
		}
		$Obj = New-Object -TypeName PSObject -Property $Properties
		$Obj
	}
}
