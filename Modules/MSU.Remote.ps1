Param(
	[Parameter(ValueFromPipeline=$True,
             ValueFromPipelineByPropertyName=$True)]
  [string[]]$FileName
)

$Changed = $False
$Success = $False
$Errors = @()
$ErrorsCount = $Global:Error.Count
$Output = @()

ForEach ($File in $FileName) {
	Try {
		$FileProp = Get-ItemProperty $File -ErrorAction Stop
		If ($FileProp.Extension -eq ".msu") {
			$CabName = ((expand.exe -D $FileProp.FullName | Select-String -NotMatch "WSUSSCAN" | Select-String .cab).Line.Split(':').TrimStart()[-1])
			$Expand = Start-Process expand.exe -Wait -Argumentlist "$($FileProp.FullName) -F:$CabName $($FileProp.DirectoryName)"
			$CabProp = Get-ItemProperty "$($FileProp.DirectoryName)\$CabName" -ErrorAction Stop
		} Else {
			$CabProp = $FileProp
		}
		$Dism = Start-Process dism.exe -PassThru -Wait -ArgumentList "/online /add-package /PackagePath:$($CabProp.FullName)"
		Switch ($Dism.ExitCode) {
			0 {
				$Changed = $True
      	$Success = $True
      	$Output += "Installed"
			}
			3010 {
				$Changed = $True
      	$Success = $True
      	$Output += "Installed/Reboot Required"
			}
			2359302 {
				$Changed = $False
      	$Success = $True
      	$Output += "Already Installed"
			}
			-2146498530 {
				$Changed = $False
				$Success = $True
				$Output += "Not Applicable"
			}
			-2145124329 {
				$Changed = $False
      	$Success = $True
      	$Output += "Not Required"
			}
			default {
				$Changed = $False
      	$Success = $False
      	$Output += "Install Failed"
				$Errors += "DISM Exited With: $($Dism.ExitCode)"
			}
		}
	}
	Catch {
		$Errors = $Global:Error[0..$($ErrorsCount - $Global:Error.Count)]
	}
	Finally {
		Remove-Item $FileProp.FullName -Force -ErrorAction SilentlyContinue
		If ($CabProp) {
			Remove-Item $CabProp.FullName -Force -ErrorAction SilentlyContinue
		}
		$Properties = @{
    	Changed = $Changed
    	Success = $Success
    	Errors = $Errors
    	Output = $Output
  	}
 		$Obj = New-Object -TypeName PSObject -Property $Properties
  	$Obj
	}
}
