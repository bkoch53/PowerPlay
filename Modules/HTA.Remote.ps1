Param(
	[Parameter(
		ValueFromPipeline=$True,
		ValueFromPipelineByPropertyName=$True)]
	[string[]]$FileName
)

$ErrorActionPreference = "Stop"

$Changed = $False
$Success = $False
$Errors = @()
$ErrorsCount = $Global:Error.Count
$Output = @()

$HTAs = $FileName
ForEach ($HTA in $HTAs) {
	Try {
		$HTA = Get-ItemProperty $HTA
		$UserName = (Get-WmiObject Win32_ComputerSystem -Property UserName).UserName
		$StartTime = (Get-Date).AddSeconds(120).ToString("HH:mm")
		$Command = "mshta.exe $($HTA.FullName)"
		$Task = & schtasks.exe /create /ru "$UserName" /rl HIGHEST /sc ONCE /tn Message /tr "$Command" /st $StartTime /f
		If (!($LastExitCode -eq 0)) {
			Throw
		}
		
		$Changed = $True
		
		$Task = & schtasks.exe /run /tn Message /I /HRESULT
		If (!($LastExitCode -eq 0)) {
			Throw
		}
		$Task = & schtasks.exe /delete /tn Message /f
		If (!($LastExitCode -eq 0)) {
			Throw
		}
		$Success = $True
		$Output += "Task successfully started, ran, and cleaned up."
	}
	Catch {
		$Errors = $Global:Error[0..$($ErrorsCount - $Global:Error.Count)]
	}
	Finally {
		$Properties = @{
			Changed = $Changed
			Success = $Success
			Errors = $Errors
			Output = $Output
		}
		$Obj = New-Object -TypeName PSObject -Property $Properties
		$Obj
	}
}
