Param(
	[Parameter(
		ValueFromPipeline=$True,
		ValueFromPipelineByPropertyName=$True)]
	[string]$Command,
	[string]$WorkingDirectory = $(pwd),
	[string]$Creates,
	[string]$Removes
)

$bytes = [System.Text.Encoding]::Unicode.GetBytes($command)
$encodedCommand = [Convert]::ToBase64String($bytes)
$NewCommand = "powershell.exe -encodedCommand $encodedCommand"

Function Parse-Command {
	Param([string]$Command)
	$Base = $Command.split().split('|').split(';')[0]
	$Properties = @{
		Application = $Base
		Arguments = $Command.replace("$Base ",'')
		Command = $Command
	}
	$Obj = New-Object -TypeName PSObject -Property $Properties
	$Obj
}

$Changed = $False
$Success = $False
$Errors = @()
$ErrorsCount = $Global:Error.Count
$Output = @()

Try {
	If ($Creates -and $(Test-Path $Creates)) {
		$Success = $True
		$Output += "Skipped, since $Creates exists"
		Return
	}
	If ($Removes -and -not $(Test-Path $Removes)) {
		$Success = $True
		$Output += "Skipped, since $Removes does not exist"
		Return
	}
	Try {
		$WorkingDirectory = Get-ItemProperty $WorkingDirectory
	} Catch {
		$Errors += "Working directory nonexistant: $WorkingDirectory"
		Return
	}

	Try {
		$ParsedCommand = Parse-Command $NewCommand
		$pinfo = New-Object System.Diagnostics.ProcessStartInfo
		$pinfo.FileName = $ParsedCommand.Application
		$pinfo.RedirectStandardError = $true
		$pinfo.RedirectStandardOutput = $true
		$pinfo.UseShellExecute = $False
		$pinfo.Arguments = $ParsedCommand.Arguments
		$pinfo.WorkingDirectory = $WorkingDirectory
		$p = New-Object System.Diagnostics.Process
		$p.StartInfo = $pinfo
		$p.Start() | Out-Null
		$p.WaitForExit()
		$stdout = $p.StandardOutput.ReadToEnd()
		$stderr = $p.StandardError.ReadToEnd()

		#$Proc = Start-Process -PassThru -Wait -WorkingDirectory $WorkingDirectory -FilePath powershell -ArgumentList "-Command $Command"

		$Output += "ExitCode: $($p.ExitCode)"
		$Output += $stdout
		$Errors += $stderr
		
		If ($Creates) {
			If (Test-Path $Creates) {
				$Success = $True
				$Changed = $True
				Return
  		} Else {
				Return
			}
		} ElseIf ($Removes) {
			If (!(Test-Path $Removes)) {
				$Success = $True
				$Changed = $True
				Return
  		} Else {
				Return
			}
		} Else {
			$Changed = $True
			Return
		}
		
	} Catch {
		Throw
	}

} Catch {
	$Errors = $Global:Error[0..$($ErrorsCount - $Global:Error.Count)]
} Finally {
	$Properties = @{
    Changed = $Changed
    Success = $Success
    Errors = $Errors
    Output = $Output
  }
  $Obj = New-Object -TypeName PSObject -Property $Properties
  $Obj
}
