Param(
	[Parameter(
		ValueFromPipeline=$True,
		ValueFromPipelineByPropertyName=$True)]
	[string[]]$Message = 'Default',
	[Parameter(
	    ValueFromPipeline=$True,
        ValueFromPipelineByPropertyName=$True)]
	[int]$Timeout = 60
)
$Changed = $False
$Success = $False
$Errors = @()
$ErrorsCount = $Global:Error.Count
$Output = @()

ForEach ($Text in $Message) {
	Try {
		$Output += msg * /TIME:$Timeout $Text 2>&1

		If ($Output.Count -eq 0) {
			$Output += "Ok"
		} Else {
			$Output += "Failed: $($Output[1])"
		}
		$Success = $True
	} Catch {
		$Errors = $Global:Error[0..$($ErrorsCount - $Global:Error.Count)]
	} Finally {
		$Properties = @{
			Changed = $Changed
			Success = $Success
			Errors = $Errors
			Output = $Output
	  }
	  $Obj = New-Object -TypeName PSObject -Property $Properties
	  $Obj
	}
}
