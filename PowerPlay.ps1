Using Module .\Computer.psm1

Param(
	[Parameter(
		Position=0,
		Mandatory=$True)]
	[string]$Playbook,
	[string]$Export = ".\$($Playbook.Split('\')[-1].Split('.')[0])_$(Get-Date -F 'ddMMMyy-HHmm')_Log.csv"
)

# R
Function ConvertTo-ComputerNameArray {
	Param(
		[Parameter(
			Position=0,
			ValueFromPipeline=$True,
			ValueFromPipelineByPropertyName=$True)]
		[string]$Value
)

	Switch -wildcard ($Value) {
		'OU=*' {
			Try {
				$SearchBase = $_
				[array]$ComputerName = Get-ADComputer `
					-SearchBase $SearchBase `
					-Filter * `
					-Properties Name | Select -ExpandProperty Name
			}
			Catch {
				Write-Warning "Failed to query ActiveDirectory"
			}
			break
		}
		'*.csv' {
			Try {
				$TestList = Get-ChildItem $Value -ErrorAction Stop
				[array]$ComputerName = Import-CSV $Value `
					-Header ComputerName | Select -Expand ComputerName -Skip 1
			}
			Catch {
				Write-Warning "Failed to import list: $Value"
			}
			break
		}
		'*.txt' {
			Try {
				$TestList = Get-ChildItem $Value -ErrorAction Stop
				[array]$ComputerName = Get-Content $Value
			}
			Catch {
				Write-Warning "Failed to import list: $Value"
			}
			break
		}
		'*,*' {
			[array]$ComputerName = $Value.Split(',')
			break
		}
		'*' {
			$ComputerName = $Value
		}
	}
	Write-Output $ComputerName.ToUpper() | Sort {Get-Random}
}

Function Export-Result {
	Param(
		[string]$Path,
		[PSObject]$InputObject
	)
	$Obj = $InputObject
	$Properties = @{
		ComputerName = $Obj.ComputerName
		Connected = $Obj.Connected
		Facts = $Obj.Facts | ConvertTo-Json -Depth 100 -Compress
		StartTime = $Obj.StartTime
		Errors = $Obj.Errors | ConvertTo-Json -Depth 100 -Compress
	}
	$Obj.Results | %{
		$Properties.$($_.Name) = $($_.Result | ConvertTo-Json -Depth 100 -Compress)
	}
	$Obj = New-Object -TypeName PSObject -Property $Properties

	While (!($ExportPass)) {
		Try {
			Export-CSV -Path $Path -InputObject $Obj -Append -NoTypeInformation -Force
			$ExportPass = $True
		}
		Catch {
			$ExportPass = $False
			Start-Sleep -s 1
		}
	}
}

Function Verify-Module {
	Param(
		$Module
	)
	Try {
		If ((Get-ItemProperty ".\Modules\$Module.*.ps1") -eq $Null) {
			Return $False
		}
		Return $True
	}
	Catch {
		Return $False
	}
}

Function Enable-WinRM {
	Param(
		[string]$ComputerName
	)
	& .\Dev\Enable-WinRM.ps1 $ComputerName -Enable
}

Try {
	$Playbook = (Get-ChildItem $Playbook).FullName
}
Catch {
	Write-Warning "File not found: $Playbook"
	Exit
}
[PSObject]$Playbook = Get-Content $Playbook | ConvertFrom-Json

# Verify All Modules
Try {
	$Modules = $Playbook.Tasks | %{
		($_|gm|Where MemberType -like "NoteP*"|Where Name -ne 'Name').Name
	}
	ForEach ($Module in $Modules) {
		If (!(Verify-Module $Module)) {
			Write-Warning "Module does not exist: $Module"
			Throw
		}
	}
}
Catch {
	Write-Warning "Missing Module"
	Exit
}

ForEach ($Play in $Playbook) {
	$ComputerName = ConvertTo-ComputerNameArray -Value $Play.Hosts
	$Headers = "ComputerName,Connected,Facts,StartTime,Errors,{0}" -f `
		"$($Play.Tasks.Name -Join ',')"
	$Headers | Out-File $Export
	ForEach ($Computer in $ComputerName) {
		Write-Progress `
			-Activity "Playbook Log: $Export" `
			-Status "$($ComputerName.IndexOf($Computer)+1) of $($ComputerName.Count)" `
			-PercentComplete $(($ComputerName.IndexOf($Computer)/$ComputerName.Count)*100)

		$Computer = [Computer]::New($Computer)
		Try {
			$Computer.InitializeConnection()
			If ($Computer.Connected) {
				If ($Play.Gather_Facts -ne $False) {
					$Computer.GetFacts()
				}
				ForEach ($Task in $Play.Tasks) {
					$Module = ($Task | gm | Where {`
						($_.MemberType -like "NoteP*") `
						-and ($_.Name -ne 'Name')`
					}).Name
					$Arguments = @{}
					$Task.$Module.psobject.properties | %{
						$Arguments.$($_.Name) = $($_.Value)
					}
					$Arguments = New-Object -TypeName PSCustomObject -Property $Arguments
					$Computer.Results += $Computer.ExecuteModule(
						$Task.Name,
						$Module,
						$Arguments
					)
				}
			} Else {
				Write-Host -F Cyan "-- Insert Error Handler Here --"
				Enable-WinRM $Computer.ComputerName
			}
		}
		Catch {
			$_
		}
		Finally {
			$Computer.ReadResults()
			Export-Result -Path $Export -InputObject $Computer
			$Computer.CloseConnection()
		}
	}
}
