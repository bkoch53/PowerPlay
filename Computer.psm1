Class Computer {
	[string]$ComputerName
	[bool]$Connected
	[psobject]$Session
	[psobject]$Facts
	[array]$Results
	[datetime]$StartTime
	[string]$TotalTime
	[psobject]$Errors

	Computer() {}
	Computer([String]$ComputerName) {
		$this.ComputerName = $ComputerName
		$this.StartTime = Get-Date
		$this.Results = @()
	}

	InitializeConnection() {
		Try {
			$this.Session = New-PSSession `
				-ComputerName $this.ComputerName `
				-Name $this.ComputerName `
				-EnableNetworkAccess `
				-ErrorAction Stop
			$this.Connected = $True
		}
		Catch {
			$this.Errors = $Global:Error[0].FullyQualifiedErrorId.Split(',')[0]
			$this.Connected = $False
		} 
	}
	
	CloseConnection() {
		Remove-PSSession -Session $this.Session
		$this.Connected = $False
		$this.Session = $Null
	}

	[PSObject]ExecuteLocalModule([string]$Module,[psobject]$Arguments) {
		$Result = $Arguments | & $Module -Computer $this 
		return $Result
	}

	[PSObject]ExecuteRemoteModule([string]$Module,[psobject]$Arguments) {
		Copy-Item `
			-ToSession $this.Session `
			-Force `
			-Path $Module `
			-Destination 'C:\Temp\' | Out-Null
		$Result = Invoke-Command `
			-Session $this.Session `
			-ArgumentList $Arguments `
			-ScriptBlock {
				Param(
					[PSObject]$Obj
				)
				$Obj.Arguments | & "C:\Temp\$($Obj.Module)"
				Remove-Item -Force -Path "C:\Temp\$($Obj.Module)"
			}
		return $Result
	}

	[PSObject]ExecuteModule([string]$Name,[string]$Module,[psobject]$Arguments) {
		$Output += @{
			Name = $Name
			Result = $Null
		}
		$ModuleProp = Get-ItemProperty ".\Modules\$Module.*.ps1" -ErrorAction Stop
		If ($ModuleProp.Name -Like "*.Local.ps1") {
			$ModuleProp = $ModuleProp | Where Name -Like "*.Local.ps1"
			$Output.Result = $this.ExecuteLocalModule($ModuleProp.FullName,$Arguments)
		} Else {
			$Arguments = @{
				Arguments = $Arguments
				Module = $ModuleProp.Name
			}
			$Output.Result = $this.ExecuteRemoteModule($ModuleProp.FullName,$Arguments)
		}
		$Output = New-Object -TypeName PSObject -Property $Output
		return $Output
	}

	[void]GetFacts() {
		$this.Facts = $this.ExecuteModule('Facts','GetFacts',$Null).Result
		#return $this.Facts
	}
	
	ReadResults(){
		$this.TotalTime = ((Get-Date) - $this.StartTime).ToString().Split('.')[0]
		If (!($this.Connected)) {
			Write-Host -F Red "Host: $($this.ComputerName)"
			Return
		}
		Write-Host -F Green "Host: $($this.ComputerName)"
		Write-Host -F Cyan "  WinVer: $($this.Facts.WindowsVersion)"
		Write-Host -F Yellow "  Total Time: $($this.TotalTime)"
		Write-Host "  Results:"
		$LargestName = ($this.Results.Name|%{$_.Length}|sort -Descending)[0]
		ForEach ($Item in $this.Results) {
			$Color = 'White'
			If ($Item.Result.Count -gt 1) {
				$Out = "    {0} :{1}:" -f `
					"$($Item.Name)",
					"$($([string]$(0..($LargestName-$Item.Name.Length) | %{
						Write-Output '-'
					})).replace(' ',''))"
				Write-Host -F $Color $Out
			}
			ForEach ($Result in $Item.Result) {
				If ($Result.Success) {
					$Color = 'Green'
				} Else {
					$Color = 'Red'
				}
				If ($Result.Changed) {
					$Color = 'Yellow'
				}
				If ($Item.Result.Count -gt 1) {
					$Out = "    :{0}: {1}" -f `
						"$($([string]$(0..($LargestName+1) | %{
							write-output '-'
						})).replace(' ',''))",
						"$($Result.Output)"
					Write-Host -F $Color $Out
					If ($Result.Success -eq $False) {
						$Out = "    :{0}: {1}" -f `
							"$($([string]$(0..($LargestName+1) | %{
								write-output '-'
							})).replace(' ',''))",
							"$($Result.Errors)"
						Write-Host -F $Color $Out
					}
				} Else {
					$Out = "    {0} :{1}: {2}" -f `
						"$($Item.Name)",
						"$($([string]$(0..($LargestName-$Item.Name.Length) | %{
							write-output '-'
						})).replace(' ',''))",
						"$($Result.Output)"
					Write-Host -F $Color $Out
					If ($Result.Success -eq $False) {
						$Out = "    :{0}: {1}" -f `
							"$($([string]$(0..($LargestName+1) | %{
								write-output '-'
							})).replace(' ',''))",
							"$($Result.Errors)"
						Write-Host -F $Color $Out
					}
				}
				$Color = 'White'
				$Result.Errors
			}
		}
	}
}
